# Go Utility Containers

These containers are designed to work in Gitlab CI as entrypoint based
pipeline containers. They are not designed to be run through ad-hoc commands
as of right now as they have defined entrypoints.

## Example Gitlab CI Configuration File

This file outlines an example of how the containers in this project can be
used together to build, test, and check for misc issues.

```yaml
stages:
  - build
  - test
  - containerize

build:
  stage: build
  image: registry.tyvoid.net/docker-base/go/gobuild
  script:
    - /entrypoint.sh
  artifacts:
    name: app
    paths:
      - bin/

check:
  stage: test
  image: registry.tyvoid.net/docker-base/go/gocheck
  script:
    - /entrypoint.sh

uint test:
  stage: test
  image: registry.tyvoid.net/docker-base/go/gotest
  script:
    - /entrypoint.sh
```
