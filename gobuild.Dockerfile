FROM golang:1.11.4-alpine

# Install protoc
ADD https://github.com/protocolbuffers/protobuf/releases/download/v3.7.1/protoc-3.7.1-linux-x86_64.zip /tmp
ADD https://github.com/upx/upx/releases/download/v3.95/upx-3.95-amd64_linux.tar.xz /tmp

ENV ERRCOLOR="\033[1;91m" \
    WRNCOLOR="\033[1;93m" \
    PRICOLOR="\033[1;92m" \
    IFOCOLOR="\033[1;96m" \
    NOCOLOR="\033[0m"

ADD scripts/setup.sh /
ADD scripts/gobuild.sh /entrypoint.sh

RUN apk add --no-cache git gcc g++ curl bash libc6-compat && \
  curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh && \
  unzip /tmp/protoc-3.7.1-linux-x86_64.zip -d /usr/local && \
  go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway && \
  go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger && \
  go get -u github.com/golang/protobuf/protoc-gen-go && \
  go get -u github.com/gobuffalo/packr/packr && \
  tar -xvf /tmp/upx-3.95-amd64_linux.tar.xz -C /tmp && \
  mv /tmp/upx-3.95-amd64_linux/upx /usr/local/bin && \
  rm -rf /tmp/* && \
  chmod +x /setup.sh /entrypoint.sh /usr/local/bin/upx

ENTRYPOINT [ "/entrypoint.sh" ]
