FROM golang:1.11.4-alpine

# Install protoc
ADD https://github.com/protocolbuffers/protobuf/releases/download/v3.7.1/protoc-3.7.1-linux-x86_64.zip /tmp

ENV ERRCOLOR="\033[1;91m" \
    WRNCOLOR="\033[1;93m" \
    PRICOLOR="\033[1;92m" \
    IFOCOLOR="\033[1;96m" \
    NOCOLOR="\033[0m"

ADD scripts/setup.sh /
ADD scripts/gocheck.sh /entrypoint.sh

RUN apk add --no-cache git curl bash gcc g++ libc6-compat && \
  curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh && \
  unzip /tmp/protoc-3.7.1-linux-x86_64.zip -d /usr/local && \
  go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway && \
  go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger && \
  go get -u github.com/golang/protobuf/protoc-gen-go && \
  go get -u github.com/gobuffalo/packr/packr && \
  wget -O - -q https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b /usr/local/bin v1.16.0 && \
  chmod +x /setup.sh /entrypoint.sh /usr/local/bin/golangci-lint && \
  golangci-lint --version

ENTRYPOINT [ "/entrypoint.sh" ]
