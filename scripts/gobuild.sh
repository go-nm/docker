#!/bin/bash

cd $CI_PROJECT_DIR

[ -f /tmp/gitlab-runner.lock ] && exit || >/tmp/gitlab-runner.lock

/setup.sh

if [ -d ./modules ]; then
  echo -e "${PRICOLOR}Building modules...${NOCOLOR}"

  cd modules
  for MOD in */; do
    go build -ldflags="-s -w" -buildmode=plugin -o ../lib/${MOD//\//}.so ./$MOD/* || exit 1
  done

  cd ..
fi

OUT_PREFIX=""
if [[ -n "${GOOS}" ]] && [[ -n "${GOARCH}" ]]; then
  OUT_PREFIX="$GOOS-$GOARCH/"
elif [[ -n "${GOOS}" ]] || [[ -n "${GOARCH}" ]]; then
  echo -e "${ERRCOLOR}ERROR: Both GOOS and GOARCH must be specified!${NOCOLOR}"
  exit 1
fi

OUT_POSTFIX=""
if [ "${GOOS}" == "windows" ]; then
  OUT_POSTFIX=".exe"
fi

if [ -f ./main.go ]; then
  echo -e "${PRICOLOR}Setting AppVersion in cmd/cmd.go...${NOCOLOR}"
  sed -ie "s/AppVersion = \"[0-9]\{1,10\}\.[0-9]\{1,10\}\.[0-9]\{1,10\}\"/AppVersion = \"${CI_COMMIT_TAG:-$CI_COMMIT_REF_NAME-$CI_JOB_ID}\"/g" main.go
  sed -ie "s/AppVersion = \"[0-9]\{1,10\}\.[0-9]\{1,10\}\.[0-9]\{1,10\}\"/AppVersion = \"${CI_COMMIT_TAG:-$CI_COMMIT_REF_NAME-$CI_JOB_ID}\"/g" cmd/cmd.go

  echo -e "${PRICOLOR}Building app binary...${NOCOLOR}"
  go build -ldflags="-s -w" -o bin/$OUT_PREFIX$CI_PROJECT_NAME$OUT_POSTFIX -v . || exit 1

  if [[ "${NO_COMPRESS}" != "true" ]]; then
    echo -e "${PRICOLOR}Compressing binary...${NOCOLOR}"
    upx --brute bin/$OUT_PREFIX$CI_PROJECT_NAME$OUT_POSTFIX
  fi

  chmod +x bin/$OUT_PREFIX$CI_PROJECT_NAME$OUT_POSTFIX
elif [ -d ./cmd/*/ ]; then
  echo -e "${PRICOLOR}Building cmd binaries...${NOCOLOR}"

  cd cmd
  for CMD in *; do
    echo -e "${PRICOLOR}Building $CMD package...${NOCOLOR}"

    go build -ldflags="-s -w" -o ../bin/$OUT_PREFIX$CMD$OUT_POSTFIX -v ./$CMD

    if [[ "${NO_COMPRESS}" != "true" ]]; then
      echo -e "${PRICOLOR}Compressing $CMD binary...${NOCOLOR}"
      upx --brute ../bin/$OUT_PREFIX$CI_PROJECT_NAME$OUT_POSTFIX
    fi

    chmod +x ../bin/$OUT_PREFIX$CMD$OUT_POSTFIX
  done
  cd ..
else
  echo -e "${WARNCOLOR}No binaries to build. Continuing...${NOCOLOR}"
fi
