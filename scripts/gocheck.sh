#!/bin/bash

cd $CI_PROJECT_DIR

[ -f /tmp/gitlab-runner.lock ] && exit || >/tmp/gitlab-runner.lock

/setup.sh

echo -e "${PRICOLOR}Running Golang CI...${NOCOLOR}"
golangci-lint run
