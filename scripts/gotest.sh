#!/bin/bash

cd $CI_PROJECT_DIR

[ -f /tmp/gitlab-runner.lock ] && exit || >/tmp/gitlab-runner.lock

/setup.sh

if [[ -z "${INTEGRATION_TEST}" ]]; then
  echo -e "${PRICOLOR}Running Unit Tests...${NOCOLOR}"
  PKG_LIST=$(go list ./... | grep -v vendor | grep -v test/integration | grep -v test/helper)

  for pkg in $PKG_LIST; do
    mkdir -p cover/$pkg
    go test -covermode=count -coverprofile "cover/$pkg.cov" $pkg
  done

  echo -e "${PRICOLOR}Merging Coverage...${NOCOLOR}"
  rm -f ./cover/coverage.cov
  COV_FILES=$(find ./cover/ -name '*.cov')
  echo "mode: count" > ./cover/coverage.cov
  for file in $COV_FILES; do
    echo $file
    tail -q -n +2 $file >> ./cover/coverage.cov
  done

  echo -e "${PRICOLOR}Generating Coverage...${NOCOLOR}"
  go tool cover -html=./cover/coverage.cov -o ./cover/coverage.html
  go tool cover -func=./cover/coverage.cov
else
  echo -e "${PRICOLOR}Running Integration Tests...${NOCOLOR}"
  go test -cover ./test/integration... || exit 1
fi
