#!/bin/bash

if [ -f $CI_PROJECT_DIR/go.mod ]; then
  echo -e "${IFOCOLOR}Skipping Go Directory Structure Build...${NOCOLOR}"
else
  # Link app to GOPATH directory
  echo -e "${PRICOLOR}Building Go Directory Structure for App...${NOCOLOR}"
  mkdir -p $GOPATH/src/gitlab.tyvoid.net/$CI_PROJECT_NAMESPACE
  cd $GOPATH/src/gitlab.tyvoid.net/$CI_PROJECT_NAMESPACE
  ln -s $CI_PROJECT_DIR
  cd $CI_PROJECT_NAME
fi

# Set up the CI Token for Private Repos
echo -e "${PRICOLOR}Setting up Private Repo Config...${NOCOLOR}"
printf 'machine gitlab.tyvoid.net\n  login gitlab-ci-token\n  password ' > ~/.netrc
echo $CI_JOB_TOKEN >> ~/.netrc

if [ -f ./Gopkg.lock ]; then
  echo -e "${WRNCOLOR}Installing Go Dep dependencies...${NOCOLOR}"
  dep ensure || exit 1
elif [ -f ./go.mod ]; then
  echo -e "${PRICOLOR}Installing Go Mod dependencies...${NOCOLOR}"
  go mod download
else
  echo -e "${WRNCOLOR}Downloading dependencies...${NOCOLOR}"
  go get ./...
fi

echo -e "${PRICOLOR}Generating code...${NOCOLOR}"
go generate -x ./... || exit 1
